import React from "react";
import SidebarNavigation from "../sidebarNavigation/sidebarNavigation";
import Header from "../header/header";

const Navigation = () => {
    return (
        <nav>
            <header className="header">
                <Header/>
            </header>
            <SidebarNavigation/>
        </nav>
    )
}

export default Navigation; 