import React from "react";

import "./header.css";

import avatar from "../../icons/avatar.svg"

const Header = () => {
  return (
    <div className="header-container">
      <div className="avatar">
        <img src={avatar} alt="avatar" />
      </div>
      <div className="header-text">
        <div className="person-name">AnimatedFred</div>
        <div>animated@demo.com</div>
      </div>
    </div>
  );
};
export default Header;
