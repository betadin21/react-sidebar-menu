import React from "react";
import "./sidebar.css";

// svg import
import { ReactComponent as Dashboard } from "../../icons/dashboard.svg";
import { ReactComponent as Revenue } from "../../icons/revenue.svg";
import { ReactComponent as Notifications } from "../../icons/nitification.svg";
import { ReactComponent as Analytics } from "../../icons/analytics.svg";
import { ReactComponent as Inventory } from "../../icons//inventory.svg";
import { ReactComponent as LightMode } from "../../icons/lite_mode.svg";
import { ReactComponent as Logout } from "../../icons/logout.svg";
import { ReactComponent as Search } from "../../icons/search.svg";
import { ReactComponent as Checkbox } from "../../icons/checkbox.svg";
import { ReactComponent as LiteIcon } from "../../icons/lite-icon.svg";

const SidebarNavigation = () => {
  // подія на переключення теми
  function handleSwitch(e) {
    e.preventDefault();
    const checkDiv = e.target.closest(".checkbox");
    checkDiv.classList.toggle("switch-c")
    const menu = checkDiv.closest(".menu");
    menu.classList.toggle("light")
    const [...links] = menu.querySelectorAll("a");
    links.forEach(el => {
      el.classList.toggle("color")
    });
    const searchBox =  menu.querySelector(".search-box")
    searchBox.classList.toggle("search-box-light")
    const svg = checkDiv.querySelector(".checkbox-icon")
    const svg2 = checkDiv.querySelector(".light-icon")
    svg.classList.toggle("hiddd")
    svg2.classList.toggle("bloccc")
  }
  return (
    <>
      <div className="menu-bar">
        <div className="menu">
          <ul className="menu-links">
            <li className="search-box nav-link">
              <Search />
              <input type="text" placeholder="Search..." />
            </li>
            <li className="nav-link">
              <a href="#">
                <Dashboard className="link-icon" />
                <span className="nav-text">Dashboard</span>
              </a>
            </li>
            <li className="nav-link">
              <a href="#">
                <Revenue className="link-icon" />
                <span className="nav-text">Revenue</span>
              </a>
            </li>
            <li className="nav-link">
              <a href="#">
                <Notifications className="link-icon" />
                <span className="nav-text">Notifications</span>
              </a>
            </li>
            <li className="nav-link">
              <a href="#">
                <Analytics className="link-icon" />
                <span className="nav-text">Analytics</span>
              </a>
            </li>
            <li className="nav-link">
              <a href="#">
                <Inventory className="link-icon" />
                <span className="nav-text">Inventory</span>
              </a>
            </li>
            <li className="nav-link bottom-link">
              <a href="#">
                <Logout className="link-icon" />
                <span className="nav-text">Logout</span>
              </a>
            </li>
            <li className="nav-link cch">
              <a href="#">
                <LightMode className="link-icon link-mode-icon" />
                <span className="nav-text">Light mode</span>
                <div className="checkbox">
                  <LiteIcon className="light-icon" onClick={handleSwitch}/>
                  <Checkbox className="checkbox-icon" onClick={handleSwitch} />
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default SidebarNavigation;
